## Yojana Authentication service for multiple project.

## Setup

Inside config/services.php, add below code.

```json
{
	"auth"  =>  [
                    "base_uri"  =>  env("AUTH_SERVICE_BASE_URL","http://localhost:8000/api/"),
                    "secret"  =>  env("AUTH_SERVICE_SECRET",""),
                ],
}
```
