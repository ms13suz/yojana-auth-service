<?php

Route::group(['namespace' => 'Yojana\AuthService\Http\Controllers'], function () {
    Route::group(['prefix' => 'api/v1'], function(){   
        Route::group(['prefix' => '/{type}'], function(){    
            Route::post('/login', ['uses' => 'AuthController@login']);
            Route::post('/get-account', ['uses' => 'AuthController@get_account']);
            Route::post('/select-account', ['uses' => 'AuthController@select_account']);
        });
    });
});
