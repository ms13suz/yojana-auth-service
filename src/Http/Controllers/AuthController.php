<?php

namespace Yojana\AuthService\Http\Controllers;

use Yojana\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yojana\AuthService\Services\AuthService;
use Yojana\AuthService\Http\Requests\AccountUuidRequest;
use Yojana\AuthService\Http\Requests\LoginRequest;
use Yojana\AuthService\Http\Requests\UserUuidRequest;

class AuthController extends Controller
{
    /**
     *  consume auth service
     * @var string
     */
    public $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     *
     * @OA\Post(
     *     path="/api/v1/{type}/login",
     *     summary="Auth / Login",
     *     tags={"Authorization"},
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="User Name/Email",
     *                     property="username",
     *                     type="string",
     *                 ),
     *                 @OA\Property(
     *                     description="Password",
     *                     property="password",
     *                     type="password",
     *                 ),
     *                 example={
     *                      "username": "yojanatech@gmail.com",
     *                      "password": "admin123"
     *                 }
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Error")
     *         ),
     *     ),
     *  *     @OA\Response(
     *         response="500",
     *         description="Internal Server Error",
     *     ),
     * )
    * @param  \Illuminate\Http\Request  $request
    *
    * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
    * @throws \Illuminate\Validation\ValidationException
    * @api                {post} /api/v1/{type}/login
    * @apiName            login
    * @apiGroup           Authorization
    * @apiVersion         1.0.0
    * @apiPermission      Authenticated User
    * @apiUse             UserResponse
    * @apiParam {String} user_id User hashed id
    * @apiParam {String} permission_id Permission hashed id
    *
    */

    public function login(LoginRequest $request)
    {
        try {

            $data = $request->only('username','password');

            $response = $this->authService->postLogin($data);

            return $this->sendResponse($response, 'You can choose your account now!');

        } catch (\Throwable $t) {
            switch ($t->getStatusCode()) {
                case 400:
                case 401:
                    return $this->sendError('Your credentials are incorrect. Please try again!',$t->getStatusCode());
                    break;
                default:
                return $this->sendError('Something went wrong on the server',$t->getStatusCode());
            }
        }
    }

    /**
     *
     * @OA\Post(
     *     path="/api/v1/{type}/get-account",
     *     summary="Auth / Get Account",
     *     tags={"Authorization"},
     *     security={ {"apiAuth": {} }},
     *     @OA\Parameter(
    *       description="Alias",
    *       in="path",
    *       name="type",
    *       required=true,
    *       example="sms",
    *       @OA\Schema(
    *           type="string",
    *         )
    *      ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="Uuid",
     *                     property="uuid",
     *                     type="string",
     *                     example="0ef36240-1353-11eb-b213-b5755ddb03be"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas")
     *         ),
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Error")
     *         ),
     *     ),
     *  *     @OA\Response(
     *         response="500",
     *         description="Internal Server Error",
     *     ),
     * )
    * @param  \Illuminate\Http\Request  $request
    *
    * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
    * @throws \Illuminate\Validation\ValidationException
    * @api                {post} /api/v1/{type}/get-account
    * @apiName            login
    * @apiGroup           Authorization
    * @apiVersion         1.0.0
    * @apiPermission      Authenticated User
    * @apiUse             UserResponse
    * @apiParam {String} user_id User hashed id
    * @apiParam {String} permission_id Permission hashed id
    *
    */

    public function get_account(UserUuidRequest $request)
    {
        try {
            // $token = $headers['token'];

            $uuid = $request->only('uuid');

            $response = $this->authService->getAccount($uuid);

            return $this->sendResponse($response, 'Account details retrieved!');

        } catch (\Throwable $t) {
            // dd(100);
            return $this->sendError($t->getMessage());
        }
    }

     /**
     *
     * @OA\Post(
     *     path="/api/v1/{type}/select-account",
     *     summary="Auth / Select Account",
     *      operationId="select-account",
     *     tags={"Authorization"},
     *     security={ {"apiAuth": {} }},
     *     @OA\Parameter(
    *       description="Alias",
    *       in="path",
    *       name="type",
    *       required=true,
    *       example="sms",
    *       @OA\Schema(
    *           type="string",
    *         )
    *      ),
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     description="User Id",
     *                     property="user_id",
     *                     type="int",
     *                     example="1"
     *                 ),
     *                  @OA\Property(
     *                     description="Uuid",
     *                     property="uuid",
     *                     type="string",
     *                     example="0ef36240-1353-11eb-b213-b5755ddb03be"
     *                 )
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @OA\Response(
     *         response="401",
     *         description="Unauthorized",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/Error")
     *         ),
     *     ),
     *  *     @OA\Response(
     *         response="500",
     *         description="Internal Server Error",
     *     ),
     * )
    * @param  \Illuminate\Http\Request  $request
    *
    * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
    * @throws \Illuminate\Validation\ValidationException
    * @api                {post} /api/v1/{type}/select-account
    * @apiName            login
    * @apiGroup           Authorization
    * @apiVersion         1.0.0
    * @apiPermission      Authenticated User
    * @apiUse             UserResponse
    * @apiParam {String} user_id User hashed id
    * @apiParam {String} permission_id Permission hashed id
    *
    */
    public function select_account(AccountUuidRequest $request)
    {
        try {
            $data = $request->only('user_id','uuid');

            $response = $this->authService->selectAccount($data);

            return $this->sendResponse($response, 'Login Successfully!');

        } catch (\Throwable $t) {
            // dd(100);
            return $this->sendError($t->getMessage());
        }
    }
}
