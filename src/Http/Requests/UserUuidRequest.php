<?php

namespace Yojana\AuthService\Http\Requests;

use Yojana\Http\Requests\BaseFormRequest;

class UserUuidRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid'=>'required|max:50',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages():array
    {
       return [];
    }
}
