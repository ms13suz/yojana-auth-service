<?php

namespace Yojana\AuthService\Http\Requests;

use Yojana\Http\Requests\BaseFormRequest;

class AccountUuidRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'uuid'=>'required|max:50',
            'user_id'=>'required|integer',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages():array
    {
       return [];
    }
}
