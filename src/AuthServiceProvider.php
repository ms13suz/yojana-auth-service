<?php

namespace Yojana\AuthService;

use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/Routes/api.php');
    }

    public function register()
    {
        // 
    }
}
