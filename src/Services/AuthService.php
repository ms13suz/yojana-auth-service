<?php

namespace Yojana\AuthService\Services;

use Yojana\Traits\ConsumeExternalService;

class AuthService
{
    use ConsumeExternalService;

    /**
     * The base uri to consume authors service
     * @var string
     */
    public $baseUri;

    /**
     * Authorization secret to pass to author api
     * @var string
     */
    public $secret;

    public function __construct()
    {
        $this->baseUri = config('services.auth.base_uri');
        // $this->secret = config('services.auth.secret');
    }

    /**
     * Login 
     */
    public function postLogin($data)
    {
        return $this->performRequest('POST','/sms/login', $data);
    }

    /**
     * Get Account
     */
    public function getAccount($data)
    {
        return $this->performRequest('POST','/sms/get-account', $data);
    }

    /**
     * Select Account
     */
    public function selectAccount($data)
    {
        return $this->performRequest('POST','/sms/select-account', $data);
    }
}